# React Native - Weather application

## Installation

1. Clone repository

git clone https://bitbucket.org/tomislav-arambasic/react-native-weather-app

2. cd into project directory

cd react-native-weather-app

3. Install all npm packages

npm install

4. If needed , link native dependecies

react-native link @react-native-community/geolocation
react-native link @react-native-community/netinfo
react-native link react-native-gesture-handler
react-native link react-native-share
react-native link react-native-vector-icons

5. Run on android emulator of physical android device

react-native run-android
