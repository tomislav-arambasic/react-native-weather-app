import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import MainScreen from './src/screens/MainScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import LocationSearchScreen from './src/screens/LocationSearchScreen';

const navigator = createStackNavigator(
  {
    Main: {
      screen: MainScreen,
      navigationOptions: {
        header: null,
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        title: 'Settings',
      },
      headerLayoutPreset: 'center',
    },
    Search: {
      screen: LocationSearchScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      headerTitleStyle: {
        flex: 1,
        textAlign: 'center',
        marginRight: 65,
      },
    },
    headerMode: 'screen',
  },
);

let Navigation = createAppContainer(navigator);

import {createStore} from 'redux';
import reducer from './src/store/reducer';
import {Provider} from 'react-redux';

const store = createStore(reducer);
// store.subscribe(() => {
//   console.log('updated');
//   console.log(store.getState());
// });

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}
