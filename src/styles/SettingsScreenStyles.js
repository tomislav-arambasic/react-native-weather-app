import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  newLocationButton: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignSelf: 'auto',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 50,
  },
  newLocationText: {
    color: 'lightblue',
    fontWeight: 'bold',
    fontSize: 25,
    alignSelf: 'center',
    marginRight: 20,
    marginLeft: 10,
  },
});
