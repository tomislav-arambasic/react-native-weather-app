import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  searchContainer: {
    flex: 1,
    backgroundColor: 'white',
    height: 40,
  },
  inputContainer: {
    backgroundColor: 'white',
    height: 30,
  },
});
