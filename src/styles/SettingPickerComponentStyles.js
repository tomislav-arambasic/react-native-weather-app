import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  settingRow: {
    flexDirection: 'row',
    borderColor: '#efefef',
    borderBottomWidth: 2,
  },
  icon: {
    width: '15%',
    alignSelf: 'center',
    paddingLeft: 20,
  },
  text: {
    width: '25%',
    paddingVertical: '3%',
    paddingLeft: '2%',
    alignContent: 'flex-start',
  },
  picker: {
    width: '60%',
  },
});
