import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  topBar: {
    flexDirection: 'row',
    paddingLeft: '4%',
  },
  buttonMargin: {marginRight: 20, marginTop: 10},
  resultList: {flex: 1, marginTop: 10, backgroundColor: '#efefef'},
});
