import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerScreen: {
    flex: 1,
  },
  statusContainer: {
    flexDirection: 'row',
    marginTop: '5%',
    height: 50,
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
  },
  iconsErrorContainer: {
    width: '50%',
    height: 50,
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  iconSettingsContainer: {
    width: '50%',
    height: 40,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  iconMargin: {
    marginLeft: 15,
  },
  containerLocation: {
    flex: 1,
    alignItems: 'center',
  },
  locationText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 13,
  },
  containerWeather: {
    alignItems: 'center',
    bottom: '40%',
  },
  weatherTemp: {
    fontSize: 60,
    color: 'white',
    fontWeight: 'bold',
  },
  weatherText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  containerSocial: {
    flex: 1,
    alignItems: 'flex-end',
  },
  bottom: {
    position: 'absolute',
    bottom: 30,
    right: 20,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  spinnerContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
