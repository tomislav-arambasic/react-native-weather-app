import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  resultItem: {
    borderBottomWidth: 1,
    borderBottomColor: 'silver',
    height: 40,
    width: '100%',
    backgroundColor: 'white',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemText: {alignItems: 'center', fontSize: 20, height: 40},
});
