import React from 'react';
import {SearchBar} from 'react-native-elements';

import styles from '../styles/SearchComponentStyles';

const Search = props => {
  return (
    <SearchBar
      containerStyle={styles.searchContainer}
      inputContainerStyle={styles.inputContainer}
      inputStyle={{fontSize: 15}}
      placeholder="Enter location..."
      onChangeText={props.onChangeTextHandler}
      value={props.searchText}
      lightTheme={true}
      showLoading={props.loading}
    />
  );
};

export default Search;
