import React from 'react';
import {Text, View, Picker} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from '../styles/SettingPickerComponentStyles';

const SettingPicker = props => {
  return (
    <View style={styles.settingRow}>
      <Icon style={styles.icon} name={props.icon} size={20} />
      <Text style={styles.text}>{props.settingLabel}</Text>
      <Picker
        selectedValue={props.selectedValue}
        style={styles.picker}
        onValueChange={itemid => props.onChange(itemid)}>
        {props.pickerItems.map(item => {
          return (
            <Picker.Item
              color="gray"
              label={item.name}
              value={item.id}
              key={item.id}
            />
          );
        })}
      </Picker>
    </View>
  );
};

export default SettingPicker;
