import React from 'react';
import {ImageBackground} from 'react-native';

const BackgroundImage = props => {
  return (
    <ImageBackground
      style={style}
      source={{
        uri: props.image,
      }}
    />
  );
};

const style = {
  height: '100%',
  width: '100%',
  position: 'absolute',
};

export default BackgroundImage;
