import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from '../styles/SearchResultComponentStyles';

const SearchResult = props => {
  if (props.results && props.results.sys) {
    return (
      <TouchableOpacity
        onPress={() => props.addNewLocationHandler(props.results.name)}
        style={styles.resultItem}>
        <Text style={styles.itemText}>
          {props.results.name}, {props.results.sys.country.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  }

  return null;
};

export default SearchResult;
