import React from 'react';
import {TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

const ShareSocial = props => {
  return (
    <TouchableOpacity onPress={props.onPressHandler} style={props.style}>
      <Icon name="md-share" size={30} color="white" />
    </TouchableOpacity>
  );
};

export default ShareSocial;
