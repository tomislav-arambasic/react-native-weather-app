import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

const StatusIcon = props => {
  if (props.signal === false)
    return <Icon name={props.icon} size={30} color="red" style={props.style} />;
  else return null;
};

export default StatusIcon;
