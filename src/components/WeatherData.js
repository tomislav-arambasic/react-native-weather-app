import React from 'react';
import {Text, View} from 'react-native';

const WeatherData = props => {
  return (
    <View style={props.containerWeatherStyle}>
      <Text style={props.weatherTempStyle}>
        {props.temperature}
        {props.tempUnits}
      </Text>
      <Text style={props.weatherTextStyle}>{props.weatherDescription}</Text>
    </View>
  );
};

export default WeatherData;
