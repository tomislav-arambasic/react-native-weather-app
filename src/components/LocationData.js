import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const LocationData = props => {
  let showUpdateText = null;

  if (props.showUpdateText === true) {
    showUpdateText = (
      <Text style={{color: 'white', marginBottom: 10, opacity: 0.7}}>
        Updated a moment ago.
      </Text>
    );
  }
  return (
    <View style={props.containerLocationStyle}>
      <Icon name="md-locate" size={20} color="white" />
      <Text style={props.locationTextStyle}>
        {props.city}, {props.country.toUpperCase()}
      </Text>
      {showUpdateText}
    </View>
  );
};

export default LocationData;
