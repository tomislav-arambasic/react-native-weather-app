const initialState = {
  imageuri: 'https://i.ibb.co/gWCFkyk/night-clearsky.png',
  city: '--',
  country: '--',
  temperature: '--',
  weatherDescription: '--',
  tempUnits: '--',
  NoGps: false,
  noNet: false,
  ready: false,
  where: {lat: null, lng: null},
  error: null,
  selectedUnits: 'Metric',
  selectedLocation: 'My current location',
  userLocations: {
    locations: [
      {
        name: 'My current location',
        id: 'My current location',
      },
      {
        name: 'Zagreb',
        id: 'Zagreb',
      },
      {
        name: 'Split',
        id: 'Split',
      },
      {
        name: 'Rijeka',
        id: 'Rijeka',
      },
      {
        name: 'Dubrovnik',
        id: 'Dubrovnik',
      },
      {
        name: 'New York',
        id: 'New York',
      },
      {
        name: 'Tokyo',
        id: 'Tokyo',
      },
      {
        name: 'New Delhi',
        id: 'New Delhi',
      },
    ],
  },
  units: [{name: 'Metric', id: 'Metric'}, {name: 'Imperial', id: 'Imperial'}],
};

const reducer = (state = initialState, action) => {
  if (action.type === 'SET_USER_UNITS') {
    state = {
      ...state,
      selectedUnits: action.payload.units,
    };
    return state;
  } else if (action.type === 'SET_GPS_LOC') {
    state = {
      ...state,
      ready: action.payload.gpsStatus.ready,
      where: action.payload.gpsStatus.where,
      error: action.payload.gpsStatus.error,
    };
    return state;
  } else if (action.type === 'SET_IMAGE_URI') {
    state = {
      ...state,
      imageuri: action.payload.imageuri,
    };
    return state;
  } else if (action.type === 'SET_WEATHER_DATA') {
    const weatherData = action.payload.weatherData;

    state = {
      ...state,
      city: weatherData.name,
      country: weatherData.sys.country,
      temperature: Math.round(weatherData.main.temp),
      tempUnits: state.selectedUnits === 'Metric' ? ' °C' : ' °F',
      weatherDescription:
        weatherData.weather[0].description.charAt(0).toUpperCase() +
        weatherData.weather[0].description.slice(1),
    };
    return state;
  } else if (action.type === 'CHANGE_USER_LOCATION') {
    state = {
      ...state,
      selectedLocation: action.payload.location,
    };
    return state;
  } else if (action.type === 'ADD_NEW_LOCATION') {
    let locations = [...state.userLocations.locations];
    locations.push(action.payload.newLocation);

    state = {
      ...state,
      userLocations: {
        ...state.userLocations,
        locations: locations,
      },
      selectedLocation: action.payload.newLocation.name,
    };
    return state;
  } else if (action.type === 'SET_LOCATIONS') {
    state = {
      ...state,
      userLocations: action.payload.locations,
    };
    return state;
  }

  return state;
};

export default reducer;
