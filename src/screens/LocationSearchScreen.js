import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';

import SearchResult from '../components/SearchResult';
import Search from '../components/Search';
import styles from '../styles/LocationSearchScreenStyles';

class LocationSearchScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      loading: false,
      results: [],
    };
  }

  onChangeTextHandler = text => {
    this.setState({
      searchText: text,
    });
    this.setState({
      loading: true,
      results: [],
    });
    setTimeout(() => {
      axios
        .get(
          `http://api.openweathermap.org/data/2.5/weather?q=${this.state.searchText}&appid=bd8a40ec6cbd84521efb760d2c51b79b`,
        )
        .then(res => {
          this.setState({
            results: res.data,
            loading: false,
          });
        })
        .catch(err => {
          this.setState({
            loading: false,
          });
          if (err.response.data.cod === 404) {
            // continue;
            // shows 404 coz API only returns exact matches, not %LIKE%
          }
        });
    }, 300);
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="ios-arrow-back"
              size={30}
              color="silver"
              style={styles.buttonMargin}
            />
          </TouchableOpacity>
          <Search
            searchText={this.state.searchText}
            loading={this.state.loading}
            onChangeTextHandler={this.onChangeTextHandler}
          />
        </View>
        <View style={styles.resultList}>
          <SearchResult
            results={this.state.results}
            addNewLocationHandler={
              this.props.navigation.state.params.addNewLocationHandler
            }
          />
        </View>
      </View>
    );
  }
}

export default LocationSearchScreen;
