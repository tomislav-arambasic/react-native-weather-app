import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import Geolocation from '@react-native-community/geolocation';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-easy-toast';

import BackgroundImage from '../components/BackgroundImage';
import ShareSocial from '../components/ShareSocial';
import StatusIcon from '../components/StatusIcon';
import LocationData from '../components/LocationData';
import WeatherData from '../components/WeatherData';
import styles from '../styles/MainScreenStyles';

import {connect} from 'react-redux';

class MainScreen extends React.Component {
  state = {
    showSpinner: false,
    networkStatus: true,
    showUpdateText: false,
  };

  componentDidMount() {
    // AsyncStorage.removeItem('location');

    this.subscribeToNetworkStatus();
    this.activateSpinner(true, 6000);
    this.getUserUnitsFromStorage();
    this.getUserLocationFromStorage();
    this.getLocationsFromStorage();
  }

  subscribeToNetworkStatus = () => {
    setInterval(() => {
      const unsubscribe = NetInfo.addEventListener(state => {
        if (!this.state.networkStatus && state.isConnected) {
          this.showErrorToast('No internet connection', 10);
          this.refreshData();
        }
        this.setState({
          networkStatus: state.isConnected,
        });
        if (state.isConnected === false) {
          this.showErrorToast('No internet connection', 500);
        }
      });
      unsubscribe();
    }, 500);
  };

  showUpdateDataText = () => {
    this.setState({
      showUpdateText: true,
    });

    setTimeout(() => {
      this.setState({
        showUpdateText: false,
      });
    }, 6000);
  };

  refreshData = () => {
    {
      this.activateSpinner(true, 6000);
      this.getUserLocationFromStorage();
    }
  };

  showErrorToast = (message, duration) => {
    this.refs.toast.show(message, duration ? duration : 5000, () => {});
  };

  activateSpinner = (active, timeout) => {
    this.setState({
      showSpinner: active,
    });
    setTimeout(() => {
      this.setState({
        showSpinner: false,
      });
    }, timeout);
  };

  getUserUnitsFromStorage = async () => {
    try {
      await AsyncStorage.getItem('selectedunits').then(userunits => {
        if (userunits) {
          this.props.setUserUnits(userunits);
        }
      });
    } catch (error) {
      this.showErrorToast(err.message);
    }
  };

  getUserLocationFromStorage = async () => {
    try {
      await AsyncStorage.getItem('location').then(loc => {
        if (!loc || loc === 'My current location') {
          this.getGeoLocationAndWeather();
        } else {
          this.onUserLocationChange(loc);
        }
      });
    } catch (error) {
      this.showErrorToast(err.message);
    }
  };

  getLocationsFromStorage = async () => {
    try {
      await AsyncStorage.getItem('userlocations').then(locations => {
        if (locations) {
          this.props.setLocationsArray(JSON.parse(locations));
        }
      });
    } catch (error) {
      this.showErrorToast(err.message);
    }
  };

  onUserLocationChange = (loc, units) => {
    this.props.changeUserLocation(loc);
    this.getWeatherData(loc, units);
  };

  getGeoLocationAndWeather = () => {
    try {
      let geoOptions = {
        enableHighAccuracy: true,
        timeOut: 20000,
      };
      this.props.setGPSlocHandler({ready: false, error: null});

      Geolocation.getCurrentPosition(
        this.geoSuccess,
        this.geoFailure,
        geoOptions,
      );
    } catch (err) {
      this.showErrorToast(err);
    }
  };

  geoSuccess = position => {
    try {
      this.props.setGPSlocHandler({
        ready: true,
        where: {lat: position.coords.latitude, lng: position.coords.longitude},
        error: null,
      });
      this.getWeatherData();
    } catch (err) {
      this.showErrorToast(err);
    }
  };

  geoFailure = err => {
    this.props.setGPSlocHandler({error: err.message});
    this.showErrorToast(err.message);
  };

  getCurrentCityTime = offset => {
    d = new Date();

    utc = d.getTime() + d.getTimezoneOffset() * 60000;

    nd = new Date(utc + 3600000 * offset);

    return nd;
  };

  getSunriseSunsetTime = (sunriseSunset, offset) => {
    var d = new Date(sunriseSunset * 1000);
    utc = d.getTime() + d.getTimezoneOffset() * 60000;

    nd = new Date(utc + (3600000 * offset) / 3600);

    return nd;
  };

  getSunsetTime = (sunset, offset) => {
    var a = new Date(sunset * 1000);
    utc = a.getTime() + a.getTimezoneOffset() * 60000;

    nd = new Date(utc + (3600000 * offset) / 3600);
    return nd;
  };

  getImageUri = (weatherDescription, sunrise, sunset, offset) => {
    const currentCityTime = this.getCurrentCityTime(offset / 3600);
    const sunriseTime = this.getSunriseSunsetTime(sunrise, offset);
    const sunsetTime = this.getSunriseSunsetTime(sunset, offset);

    const gtSunrise = currentCityTime > sunriseTime;
    const ltSunset = currentCityTime < sunsetTime;

    if (gtSunrise && ltSunset) {
      if (weatherDescription === 'Partly cloudy')
        this.props.setImageUriHandler(
          'https://i.ibb.co/Bn6pB5w/day-partlycloudy.png',
        );
      else if (weatherDescription.includes('cloud'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/y5c0dV1/day-cloudy.png',
        );
      else if (weatherDescription.includes('clear'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/gWCFkyk/day-clearsky.png',
        );
      else if (
        weatherDescription.includes('fog') ||
        weatherDescription.includes('mist') ||
        weatherDescription.includes('haze')
      )
        this.props.setImageUriHandler('https://i.ibb.co/bWBBXQv/day-fog.png');
      else if (weatherDescription.includes('rain'))
        this.props.setImageUriHandler('https://i.ibb.co/Yb53y5F/day-rain.png');
      else if (weatherDescription.includes('snow'))
        this.props.setImageUriHandler('https://i.ibb.co/1Z4vKgQ/day-snow.png');
    } else {
      if (weatherDescription === 'Partly cloudy')
        this.props.setImageUriHandler(
          'https://i.ibb.co/k2Xq2W1/night-partlycloudy.png',
        );
      else if (weatherDescription.includes('cloud'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/020t6BL/night-cloudy.png',
        );
      else if (weatherDescription.includes('clear'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/MZ9p4RK/night-clearsky.png',
        );
      else if (
        weatherDescription.includes('fog') ||
        weatherDescription.includes('mist') ||
        weatherDescription.includes('haze')
      )
        this.props.setImageUriHandler(
          'https://i.ibb.co/bWBBXQv/night-cloudy.png',
        );
      else if (weatherDescription.includes('rain'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/TkWVzjx/night-rain.png',
        );
      else if (weatherDescription.includes('snow'))
        this.props.setImageUriHandler(
          'https://i.ibb.co/SNTNhPv/night-snow.png',
        );
      else
        this.props.setImageUriHandler(
          'https://i.ibb.co/gWCFkyk/day-clearsky.png',
        );
    }
  };

  getWeatherData = (location, units) => {
    const {lat, lng} = this.props.state.where;
    if (!units) {
      units = this.props.state.selectedUnits;
    }

    if (location) {
      axios
        .get(
          `http://api.openweathermap.org/data/2.5/weather?q=${location}&units=${units}&appid=bd8a40ec6cbd84521efb760d2c51b79b`,
        )
        .then(res => {
          this.activateSpinner(false);
          const APIweatherData = res.data;
          console.log(APIweatherData);
          this.getImageUri(
            APIweatherData.weather[0].description,
            APIweatherData.sys.sunrise,
            APIweatherData.sys.sunset,
            APIweatherData.timezone,
          );
          this.props.updateWeatherData(APIweatherData);
          this.showUpdateDataText();
        })
        .catch(err => {
          this.activateSpinner(false);
          this.showErrorToast(err.message);
        });
    } else {
      axios
        .get(
          `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=${units}&appid=bd8a40ec6cbd84521efb760d2c51b79b`,
        )
        .then(res => {
          this.activateSpinner(false);
          const APIweatherData = res.data;
          console.log(APIweatherData);
          this.getImageUri(
            APIweatherData.weather[0].description,
            APIweatherData.sys.sunrise,
            APIweatherData.sys.sunset,
            APIweatherData.timezone,
          );
          this.props.updateWeatherData(APIweatherData);
          this.showUpdateDataText();
        })
        .catch(err => {
          this.activateSpinner(false);
          this.showErrorToast(err.message);
        });
    }
  };

  onSharePressHandler = () => {
    const shareOptions = {
      title: 'Cool story bro',
      message: `Current temperature for ${this.props.state.city} is ${this.props.state.temperature} ${this.props.state.tempUnits}, ${this.props.state.weatherDescription}`,
    };
    Share.open(shareOptions)
      .then(res => {
        // console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };

  render() {
    const state = this.props.state;

    return (
      <View style={styles.containerScreen}>
        <BackgroundImage
          weather={state.weatherDescription}
          image={state.imageuri}
        />
        <View style={styles.statusContainer}>
          <View style={styles.iconsErrorContainer}>
            <StatusIcon
              icon="ios-wifi"
              style={styles.iconMargin}
              signal={this.state.networkStatus}
            />
            <StatusIcon
              icon="imd-locate"
              style={styles.iconMargin}
              signal={this.state.NoGps}
            />
          </View>
          <View style={styles.iconSettingsContainer}>
            <ShareSocial
              onPressHandler={this.onSharePressHandler}
              style={styles.iconMargin}
            />
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate({
                  routeName: 'Settings',
                  params: {
                    getGeoLocationAndWeather: this.getGeoLocationAndWeather,
                    onUserLocationChange: this.onUserLocationChange,
                  },
                })
              }>
              <Icon
                name="ios-settings"
                size={30}
                color="white"
                style={styles.iconMargin}
              />
            </TouchableOpacity>
          </View>
        </View>
        <LocationData
          containerLocationStyle={styles.containerLocation}
          locationTextStyle={styles.locationText}
          city={state.city}
          country={state.country}
          showUpdateText={this.state.showUpdateText}
        />
        <WeatherData
          containerWeatherStyle={styles.containerWeather}
          weatherTempStyle={styles.weatherTemp}
          weatherTextStyle={styles.weatherText}
          temperature={state.temperature}
          tempUnits={state.tempUnits}
          weatherDescription={state.weatherDescription}
        />
        <View style={styles.spinnerContainer}>
          <Spinner
            visible={this.state.showSpinner}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
          />
        </View>
        <Toast ref="toast" style={{backgroundColor: '#f45340'}} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    state: state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUserUnits: units =>
      dispatch({
        type: 'SET_USER_UNITS',
        payload: {units: units},
      }),

    changeUserLocation: location =>
      dispatch({
        type: 'CHANGE_USER_LOCATION',
        payload: {
          location,
        },
      }),

    updateWeatherData: weatherData =>
      dispatch({
        type: 'SET_WEATHER_DATA',
        payload: {
          weatherData,
        },
      }),
    setGPSlocHandler: gpsStatus =>
      dispatch({
        type: 'SET_GPS_LOC',
        payload: {
          gpsStatus,
        },
      }),
    setImageUriHandler: imageuri =>
      dispatch({
        type: 'SET_IMAGE_URI',
        payload: {
          imageuri,
        },
      }),
    setLocationsArray: locations =>
      dispatch({
        type: 'SET_LOCATIONS',
        payload: {
          locations,
        },
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainScreen);
