import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
// import Icon from 'react-native-vector-icons/Ionicons';

import SettingPicker from '../components/SettingPicker';
import styles from '../styles/SettingsScreenStyles';

import {connect} from 'react-redux';

class SettingsScreen extends React.Component {
  saveToStorage = async (key, value) => {
    await AsyncStorage.setItem(key, value.toString());
  };

  changeLocationHandler = selectedLocation => {
    this.props.changeUserLocation(selectedLocation);
    this.updateWeatherData(selectedLocation, null);

    this.saveToStorage('location', selectedLocation.toString());
  };

  changeUnitsHandler = selectedUnits => {
    this.props.changeUserUnits(selectedUnits);
    this.saveToStorage('selectedunits', selectedUnits.toString());

    this.updateWeatherData(this.props.state.selectedLocation, selectedUnits);
  };

  updateWeatherData = (location, units) => {
    if (location === 'My current location')
      this.props.navigation.state.params.getGeoLocationAndWeather();
    else
      this.props.navigation.state.params.onUserLocationChange(location, units);
  };

  addNewLocationHandler = newLocation => {
    // update user locations
    // update selected location
    // get new weather data

    // todo: save new locations list to storage + check if location exists in list alredy
    const newLocationObj = {id: newLocation, name: newLocation};
    this.updateWeatherData(newLocation, null);
    this.props.updateNewLocation(newLocationObj);

    const currentLocations = [...this.props.state.userLocations.locations];
    currentLocations.push(newLocationObj);
    const newLocationsObj = {
      locations: [...currentLocations],
    };
    this.saveToStorage('userlocations', JSON.stringify(newLocationsObj));

    this.props.navigation.navigate('Settings');
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <SettingPicker
          icon="md-locate"
          settingLabel="Location"
          selectedValue={this.props.state.selectedLocation}
          onChange={this.changeLocationHandler}
          pickerItems={this.props.state.userLocations.locations}
        />
        <SettingPicker
          icon="md-speedometer"
          settingLabel="Units"
          selectedValue={this.props.state.selectedUnits}
          onChange={this.changeUnitsHandler}
          pickerItems={this.props.state.units}
        />
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate({
              routeName: 'Search',
              params: {
                addNewLocationHandler: this.addNewLocationHandler,
              },
            })
          }
          style={styles.newLocationButton}>
          {/* <Icon name="ios-add" color="lightblue" size={60} /> */}
          <Text style={styles.newLocationText}>Add new location</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    state: state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeUserLocation: location =>
      dispatch({
        type: 'CHANGE_USER_LOCATION',
        payload: {location},
      }),
    changeUserUnits: units =>
      dispatch({
        type: 'SET_USER_UNITS',
        payload: {units},
      }),
    updateNewLocation: newLocation =>
      dispatch({
        type: 'ADD_NEW_LOCATION',
        payload: {newLocation},
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsScreen);
